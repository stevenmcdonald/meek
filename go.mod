module gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/meek.git

go 1.13

require (
	github.com/refraction-networking/utls v0.0.0-20210713165636-0b2885c8c0d4
	gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/goptlib v1.4.0
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0
	golang.org/x/sys v0.0.0-20200428200454-593003d681fa
)
